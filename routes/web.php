<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'judul'], function () {
    Route::get('/', 'JudulController@index');
    Route::post('/add', 'JudulController@form');
    Route::post('/save', 'JudulController@save');
    Route::get('/show', 'JudulController@show');
    Route::post('/delete', 'JudulController@delete');
    Route::get('/datatable','JudulController@datatable')->name('judul_datatable');
});
