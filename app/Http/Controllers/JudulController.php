<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Judul;
use Yajra\DataTables\DataTables;
use DB;


class JudulController extends Controller
{
    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $judul = Judul::all();
        
        // return view('dataMaster/judul/index', ['judul' => $judul]);
        return view('dataMaster/judul/index');
    }

    public function form(Request $request)
    {
        $id = $request->input('id');
        $data_judul = Judul::all();

        if($id) {
            $data = Judul::find($id);
        } else
        {
            $data = new Judul();
        }
        $params =[
            'title' => 'Manajemen Functional Location',
            'data' => $data,
            'data_judul' => $data_judul
        ];
        return view('dataMaster.judul.formMaster', $params);
    }

    public function save(Request $request)
    {
        $id = intval($request->input('id', 0));

        if($id) {
            $data = Judul::find($id);
        } else
        {
            $data = new Judul();
            $checkData = Judul::where(['JUDUL' => $request->JUDUL])->first();
            if($checkData){
                return "<div class='alert alert-danger'>Data sudah tersedia!</div>";
            }
        }

        $data->JUDUL = $request->JUDUL;
        $data->PENGARANG = $request->PENGARANG;
        $data->TAHUN = $request->TAHUN;
        $data->URAIAN = $request->URAIAN;
        
        try {
            $data->save();
            return "
            <div class='alert alert-success'> Add Judul Success!</div>
            <script> scrollToTop(); reload(1500); </script>";
        } catch (\Exception $ex){
            return "<div class='alert alert-danger'>Add Judul Failed! Judul not saved!</div>";
        }
    }

    public function datatable(Request $request)
    {
        DB::statement(DB::raw("set @rownum=0"));
        $data = Judul::select([
            DB::raw("@rownum :=@rownum+1 as row_number"),
            'id',
            'JUDUL',
            'PENGARANG',
            'TAHUN',
            'URAIAN'
        ]);

        return DataTables::of($data)
        ->addColumn('action', function($list) {
            return "<a class='btn btn-sm btn-success btn-rounded' onclick='loadModal(this)' target='/judul/add' data='id=".$list->id."' title='Edit'>
                        <span class='fa fa-pencil-ruler' style='color: white'></span>
                    </a>
                    <a class='btn btn-danger btn-sm btn-rounded' onclick=deleteData(".$list->id.") title='Hapus'>
                        <span class='fa fa-trash' style='color: white'></span>
                    </a>";
        })
        ->filter(function($query) use ($request){
            if($request->input('search')['value']){
                $query->where('JUDUL','like',"%{$request->input('search')['value']}%");
            }
        })->make(true);
    }
}
