<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('home') }}"><img src="{{asset('assets/img/penslogo.png')}}" class="img-responsive" alt="logo" width="40"> PERPUSTAKAAN PENS</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <!-- <a href="{{ route('home') }}">SHE</a> -->
            <a href="{{ route('home') }}"><img src="{{asset('assets/img/penslogo.png')}}" class="img-responsive" alt="logo" width="40"></a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="{{ request()->is('home') ? 'active' : '' }}">
                <a href="{{ route('home') }}" class="nav-link"><i class="fas fa-th-large"></i><span>Dashboard</span></a>
            </li>

            <li class="menu-header">Master Data</li>
            <li class="{{ request()->is('buku') ? 'active' : '' }}">
                <a href="{{ url('/buku') }}" ><i class="fas fa-address-book"></i> <span>Buku</span></a>
            </li>
            <li class="{{ request()->is('judul') ? 'active' : '' }}">
                <a href="{{ url('/judul') }}" ><i class="fas fa-book"></i> <span>Judul</span></a>
            </li>
        </ul>
    </aside>
</div>
