@extends('layouts.app')
@section('content')
<title>Judul | Perpustakaan PENS</title>
<section class="section">
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>List Master Judul Perpustakaan PENS.</h4>
                    </div>
                    <!-- <p class="section-lead">
                        <a href="{{ url('accidentreportcreate') }}" class="btn btn-icon icon-left btn-primary"><i
                        class="far fa-edit"></i> Create New</a>
                    </p> -->
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped" style="width:100%" id="judulTable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Judul Buku</th>
                                        <th>Pengarang</th>
                                        <th>Tahun Terbit</th>
                                        <th>Uraian</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@section('scripts')
    <script>

        var table = $('#judulTable').DataTable({
            processing: true,
            serverSide: true,
            autoWidth: true,
            ajax:{
                url:"{{route('judul_datatable')}}",
                type:"GET",
            },
            order:[1,'desc'],
                columns:[
                    {data: 'row_number', name: 'row_number', orderable:false },
                    {data:'JUDUL', name:'JUDUL'},
                    {data:'PENGARANG', name:'PENGARANG'},
                    {data:'TAHUN', name:'TAHUN'},
                    {data:'URAIAN', name:'URAIAN'},
                    {data: 'action', name: 'action', orderable:false, searchable:false },
                ],
        });
    </script>
@endsection
@endsection