<div id="result-form-konten">
    <form onsubmit="return false;" id="form-konten" class='form-horizontal'>
        <table width="100%">
            <tbody>
                <tr>
                    <td class="text-center">
                        <img src="assets/img/penslogo.png" class="img-responsive" alt="logo" width="80">
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <h6 class="modal-title" id="title">Form Master Judul Perpustakaan</h6>
                        <h4 class="modal-title" id="title"></h4><small class="font-bold">Politeknik Elektronika Negeri Surabaya</small>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="modal-body" style="overflow:auto; height:400px">
            <hr>
            <!-- <form class="form-horizontal" role="form"> -->
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type='hidden' name='_token' value='{{csrf_token()}}'>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="id">ID:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="fid" disabled value="{{ $data->id }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Judul Buku:</label>
                    <div class="col-sm-10">
                        <input type="text" name="JUDUL" value="{{ $data->JUDUL }}" class="form-control input-sm touchspin">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Pengarang:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="PENGARANG" value="{{$data->PENGARANG}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Tahun Terbit:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="TAHUN" value="{{$data->TAHUN}}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="nickname">Uraian:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nn" name="URAIAN" value="{{$data->URAIAN}}">
                    </div>
                </div>
            <!-- </form> -->

            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-info" data-loading-text="<i class='fa fa-spinner fa-spin'></i>
                    &nbsp;<font class='lowercase'></font>"> <i class="fa fa-check"></i> Save</button>
            </div>
        </div>

        <input type='hidden' name='id' value='{{ $data->id }}'>
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
    </form>
</div>

<script>
    $(document).ready(function () {
        $('#form-konten').submit(function () {
            var data = getFormData('form-konten');
            ajaxTransfer('/judul/save', data, '#result-form-konten');
        })
    })
</script>
